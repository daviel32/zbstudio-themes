local function h2d(n) return 0+('0x'..n) end
local function H(c, bg) c = c:gsub('#','')
  -- since alpha is not implemented, convert RGBA to RGB
  -- assuming 0 is transparent and 255 is opaque
  -- based on http://stackoverflow.com/a/2645218/1442917
  local bg = bg and H(bg) or {255, 255, 255}
  local a = #c > 6 and h2d(c:sub(7,8))/255 or 1
  local r, g, b = h2d(c:sub(1,2)), h2d(c:sub(3,4)), h2d(c:sub(5,6))
  return {
    math.min(255, math.floor((1-a)*bg[1]+a*r)),
    math.min(255, math.floor((1-a)*bg[2]+a*g)),
    math.min(255, math.floor((1-a)*bg[3]+a*b))}
end

local C = {
  Background  = H'fcfcfc',
  CurrentLine = H'fcfcfc',
  Selection   = H'e3e3e3',
  Foreground  = H'555555',
  Comment     = H'888888',
  Linenumber  = H'4c5463',
  Brackets    = H'a9b1bd',
  Red         = H'e76c76',
  Orange      = H'D86C00',
  Yellow      = H'eab700',
  Green       = H'5cc66f',
  Blue        = H'61afef',
  Purple      = H'c978d9',
  Teal        = H'44b5c0',
  Pink        = H'ea316e',
}

return {
  colors = C,
  -- wxstc.wxSTC_LUA_DEFAULT
  lexerdef = {fg = C.Foreground, b = true},
  -- wxstc.wxSTC_LUA_COMMENT, wxstc.wxSTC_LUA_COMMENTLINE, wxstc.wxSTC_LUA_COMMENTDOC
  comment = {fg = C.Comment, fill = true},
  -- wxstc.wxSTC_LUA_STRING, wxstc.wxSTC_LUA_CHARACTER, wxstc.wxSTC_LUA_LITERALSTRING
  stringtxt = {fg = C.Green, b = true},
  -- wxstc.wxSTC_LUA_STRINGEOL
  stringeol = {fg = C.Green, fill = true},
  -- wxstc.wxSTC_LUA_PREPROCESSOR
  preprocessor = {fg = C.Orange},
  -- wxstc.wxSTC_LUA_OPERATOR
  operator = {fg = C.Foreground, b = true},
  -- wxstc.wxSTC_LUA_NUMBER
  number = {fg = C.Orange, b = false},
  
  -- wxstc.wxSTC_LUA_WORD, wxstc.wxSTC_LUA_WORD2-8
  keywords0 = {fg = C.Purple, b = true},
  keywords1 = {fg = C.Orange, b = true},
  keywords2 = {fg = C.Teal, b = true},
  keywords3 = {fg = C.Purple, b = true},
  keywords4 = {fg = C.Purple, b = true},
  keywords5 = {fg = C.Purple, b = true},
  keywords6 = {fg = C.Purple, b = true},
  keywords7 = {fg = C.Purple, b = true},

  -- common (inherit fg/bg from text)
  -- wxstc.wxSTC_LUA_IDENTIFIER
  text = {fg = C.Foreground, bg = C.Background, b = true},
  linenumber = {fg = C.Linenumber},
  bracematch = {fg = C.Orange, b = true},
  bracemiss = {fg = C.Pink, b = true},
  ctrlchar = {fg = C.Yellow},
  indent = {fg = C.Background},
  calltip = {fg = C.Foreground},

  -- common special (need custom fg & bg)
  sel = {bg = C.Selection},
  caret = {fg = C.Foreground},
  caretlinebg = {bg = C.CurrentLine},
  fold = {fg = C.Comment, bg = C.Background, sel = C.Comment},
  whitespace = {fg = C.Comment},
  edge = {},

  indicator = {
    fncall = {fg = C.Blue, st = wxstc.wxSTC_INDIC_TEXTFORE},
    --varlocal = {fg = C.Blue, st = wxstc.wxSTC_INDIC_TEXTFORE},
    --varglobal = {fg = C.Blue, st = wxstc.wxSTC_INDIC_TEXTFORE},
    varmasking = {fg = C.Blue, st = wxstc.wxSTC_INDIC_TEXTFORE},
    varmasked = {fg = C.Blue, st = wxstc.wxSTC_INDIC_TEXTFORE},
    varself = {fg = C.Orange, st = wxstc.wxSTC_INDIC_TEXTFORE},
    searchmatch = {fg = C.Selection, st = wxstc.wxSTC_INDIC_TEXTFORE}
  },

  -- markers
  marker = {
    message = {bg = C.Selection},
    output = {bg = C.CurrentLine},
    prompt = {fg = C.Foreground, bg = C.Background},
    error = {bg = C.Background},
  },
}
