# zbstudio-themes

1. Download the themes and put them in your zbstudio path under the cfg directory
2. Start zbstudio and click on Edit -> Ppreferences -> settings:user
3. Put the following in to that file so it looks like the following:

```lua
-- use either love2d-light.lua or love2d-dark.lua
styles = loadfile('cfg/love2d-light.lua')()
stylesoutshell = styles
styles.auxwindow = styles.text
styles.calltip = styles.text

-- these lines are optional and only usefule in combination with plugins. Uncomment them for activation
-- highlightfunctioncalls = {indicator = wxstc.wxSTC_INDIC_TEXTFORE, color = styles.colors.Blue}
-- highlightproperties = {indicator = wxstc.wxSTC_INDIC_TEXTFORE, color = styles.colors.Red}
```
4. Optional: Add this plugins to zbstudio: https://gitlab.com/daviel32/zbstudio-plugins